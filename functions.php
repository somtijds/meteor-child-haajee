<?php 

include_once( 'inc/theme-setup.php' );
include_once( 'inc/customizer.php' );
include_once( 'inc/template-tags.php' );

function haajee_include_cmb_fields() {
	if ( is_plugin_active( 'cmb2' ) ) {
		include_once( 'inc/cmb-functions.php' );		
	}
}
add_action( 'admin_init', 'haajee_include_cmb_fields' );

/**
 * Render infinite posts by using template parts
 */
function haajee_render_infinite_posts() {
	while ( have_posts() ) {
		the_post();

		if ( is_search() ) {
			get_template_part( 'template-parts/content-search' );
		} else {
			get_template_part( 'template-parts/content-jetpack-portfolio' );
		}
	}
}