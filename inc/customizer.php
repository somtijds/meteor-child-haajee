<?php
/**
 * Adjust header height based on theme option
 */
function haajee_css_output() {
	// Theme Options
	$background_color  = get_theme_mod( 'background_color', '#ffff00' );

	// Check for styles before outputting
	if ( $background_color ) {
	wp_enqueue_style( 
		'meteor-child-haajee-style', 
		get_stylesheet_directory_uri(). '/assets/dist/css/meteor-child-haajee.css' 
	);

    $meteor_child_custom_css = "

	.site-title a, .haajee_page-title {
        background-color: #$background_color;
    }

	";
	wp_add_inline_style( 'meteor-child-haajee-style', $meteor_child_custom_css );
} }
add_action( 'wp_enqueue_scripts', 'haajee_css_output' );