<?php

/**
 * Load the parent and child theme styles
 */

function haajee_enqueue_parent_style() {
	// Parent theme styles
	wp_enqueue_style( 'meteor-style', get_template_directory_uri(). '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'haajee_enqueue_parent_style' );

/**
 * Dequeue parent theme's scripts and enqueue customized versions
 *
 * @return void
 */
function haajee_dequeue_and_enqueue_scripts() {

	// Parent theme script
	wp_dequeue_script( 'meteor-js' );
	
	if ( is_page ( 154 ) ) {
		wp_enqueue_script( 'images-loaded', 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js', array( 'jquery' ), NULL, false );
		// Child theme script
		wp_enqueue_script( 'meteor-child-haajee-loader-js', get_stylesheet_directory_uri() . '/assets/dist/js/meteor-child-haajee-loader.js', array( 'jquery', 'images-loaded' ), NULL, false );
	}
	
	wp_enqueue_script( 'meteor-child-haajee-js', get_stylesheet_directory_uri() . '/assets/dist/js/meteor-child-haajee.js', array(), NULL, true );


}
add_action( 'wp_enqueue_scripts', 'haajee_dequeue_and_enqueue_scripts', PHP_INT_MAX );

/**
 * Remove unnecessary theme support entries and add customized ones
 *
 * @return void
 */
function haajee_meteor_child_setup() {

	remove_theme_support( 'automatic-feed-links' );

	// Portfolio Rectangle
	remove_image_size( 'meteor-portfolio-blocks' );

	// Portfolio Square
	remove_image_size( 'meteor-portfolio-masonry' );

	// Remove existing portfolio-carousel size.
	remove_image_size( 'meteor-portfolio-carousel' );
	// Portfolio Carousel
	add_image_size( 'meteor-portfolio-carousel', 1072, 804, true );

	// Logo size
	remove_image_size('meteor-logo');

	/**
	 * Enable post formats
	 */
	remove_theme_support( 'post-formats' );

	$removed = remove_theme_support( 'infinite-scroll' );

	if ( $removed ) {
		add_theme_support( 
			'infinite-scroll', 
			[
				'type' => 'scroll',
				'container' => 'section-portfolio',
				'footer_widgets' => false,
				'footer' => false,
				'wrapper' => false,
				'render' => 'haajee_render_infinite_posts',
				'posts_per_page' => 6,
			]
		);
	}

}
add_action( 'after_setup_theme', 'haajee_meteor_child_setup', 11 );

/**
 * Register contact page sidebar
 *
 * @return void
 */
function haajee_register_contact_page_sidebar() {
	register_sidebar(array(
		'name' => 'Haajee Contactpagina',
		'id' => 'haajee_contact_page_widgets',
	));
}
add_action( 'widgets_init', 'haajee_register_contact_page_sidebar' );

/**
 * Register Haajee's font stylesheet
 *
 * @return void
 */
function haajee_fonts_url() {
	?>
 	<script src="https://use.typekit.net/wqj7hpu.js"></script>
		<script>
			try{
				Typekit.load({
					async: true }
				);}
			catch(e){
				console.log( e );
			}
	</script>
	<?php
}
add_action( 'wp_head', 'haajee_fonts_url' );

/**
 * Add page-attributes and taxonomies to the jetpack-portfolio post type
 *
 * @return void
 */
function haajee_change_jetpack_portfolio_object() {
	register_taxonomy( 'jetpack-portfolio-location', 'jetpack-portfolio', array(
		'hierarchical'      => false,
		'labels'            => array(
			'name'                       => esc_html__( 'Projectlocatie(s)',                   'haajee' ),
			'singular_name'              => esc_html__( 'Projectlocatie',                    'haajee' ),
			'menu_name'                  => esc_html__( 'Project Locaties',                   'haajee' ),
			'all_items'                  => esc_html__( 'Alle Projectlocaties',              'haajee' ),
			'edit_item'                  => esc_html__( 'Wijzig Projectlocatie',             'haajee' ),
			'view_item'                  => esc_html__( 'Bekijk Projectlocatie',             'haajee' ),
			'update_item'                => esc_html__( 'Update Projectlocatie',             'haajee' ),
			'add_new_item'               => esc_html__( 'Voeg nieuwe Projectlocatie toe',    'haajee' ),
			'new_item_name'              => esc_html__( 'Nieuwe projectlocatie Naam',        'haajee' ),
			'search_items'               => esc_html__( 'Zoek Projectlocaties',              'haajee' ),
			'popular_items'              => esc_html__( 'Populaire Projectlocaties',         'haajee' ),
			'separate_items_with_commas' => esc_html__( 'Meerdere locaties? Gebruik een komma!',  'haajee' ),
			'add_or_remove_items'        => esc_html__( 'Projectlocaties toevoegen of verwijderen', 'haajee' ),
			'choose_from_most_used'      => esc_html__( 'Kies uit de meest gebruikte projectlocaties', 'haajee' ),
			'not_found'                  => esc_html__( 'Geen projectlocaties gevonden',     'haajee' ),
			'items_list_navigation'      => esc_html__( 'Projectlocatie list navigation',    'haajee' ),
			'items_list'                 => esc_html__( 'Projectlocatie list',               'haajee' ),
		),
		'public'            => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'project-location' ),
	) );
	register_taxonomy( 'jetpack-portfolio-status', 'jetpack-portfolio', array(
		'hierarchical'      => false,
		'labels'            => array(
			'name'                       => esc_html__( 'Projectstatus(sen)',                   'haajee' ),
			'singular_name'              => esc_html__( 'Project Status',                    'haajee' ),
			'menu_name'                  => esc_html__( 'Projectstatus',                   'haajee' ),
			'all_items'                  => esc_html__( 'Alle Projectstatussen',              'haajee' ),
			'edit_item'                  => esc_html__( 'Wijzig Projectstatus',             'haajee' ),
			'view_item'                  => esc_html__( 'Bekijk Projectstatus',             'haajee' ),
			'update_item'                => esc_html__( 'Update Projectstatus',             'haajee' ),
			'add_new_item'               => esc_html__( 'Voeg nieuwe Projectstatus toe',    'haajee' ),
			'new_item_name'              => esc_html__( 'Nieuwe projectstatus Naam',        'haajee' ),
			'search_items'               => esc_html__( 'Zoek Projectstatus(sen)',              'haajee' ),
			'popular_items'              => esc_html__( 'Populaire Projectstatus(sen)',         'haajee' ),
			'separate_items_with_commas' => esc_html__( 'Meerdere statussen? Gebruik een komma! ;)',  'haajee' ),
			'add_or_remove_items'        => esc_html__( 'Projectstatus(sen) toevoegen of verwijderen', 'haajee' ),
			'choose_from_most_used'      => esc_html__( 'Kies uit de meest gebruikte projectstatussen', 'haajee' ),
			'not_found'                  => esc_html__( 'Geen projectstatussen gevonden',     'haajee' ),
			'items_list_navigation'      => esc_html__( 'Projectstatus-lijstnavigatie',    'haajee' ),
			'items_list'                 => esc_html__( 'Projectstatus-lijst',               'haajee' ),
		),
		'public'            => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'project-status' ),
	) );
	add_post_type_support( 'jetpack-portfolio','page-attributes' );
	register_taxonomy_for_object_type('jetpack-portfolio-location','jetpack-portfolio');
	register_taxonomy_for_object_type('jetpack-portfolio-status','jetpack-portfolio');
}
add_action('init','haajee_change_jetpack_portfolio_object' );


/**
 * Make sure that portfolio is changed to projecten, so that we can use the archive function + infinite scroll.
 *
 * @param [type] $args
 * @param [type] $post_type
 * @return array $args
 */
function haajee_alter_portfolio_archive_slug($args, $post_type) {
	if ( 'jetpack-portfolio' !== $post_type ) {
		return $args;
	}
	$args['rewrite']['slug'] = 'projecten';

	return $args;
}
add_filter( 'register_post_type_args', 'haajee_alter_portfolio_archive_slug', 10, 2);

function haajee_alter_portfolio_query( $query ) {

	if ( $query->is_main_query() && $query->is_post_type_archive('jetpack-portfolio')) {
		$query->set( 'orderby', 'menu_order' );
		$query->set( 'order', 'ASC' );
	}
}
add_action( 'pre_get_posts', 'haajee_alter_portfolio_query' );

/**
 * Sort all Infinite Scroll results alphabetically by menu order
 *
 * @param array $args
 * @filter infinite_scroll_query_args
 * @return array
 */
function jetpack_infinite_scroll_query_args( $args ) {
	// if (!is_post_type_archive('jetpack-portfolio')) {
	// 	return $args;
	// }
	$args['order'] = 'ASC';
	$args['orderby'] = 'menu_order';
	return $args;
   }
add_filter( 'infinite_scroll_query_args', 'jetpack_infinite_scroll_query_args' );

add_filter('infinite_scroll_query_object', static function( $wpQuery ) {
	return $wpQuery;
});


add_filter ( 'infinite_scroll_is_last_batch', static function( $isLastBatch ) {
	if (is_post_type_archive('jetpack-portfolio')) {
		return false;
	}
	return $isLastBatch;
});