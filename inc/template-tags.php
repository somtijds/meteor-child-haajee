<?php
/**
 * Haajee overrides for Meteor template tags.
 */

/**
 * Site title and logo
 *
 * @return void
 */
function meteor_title_logo() {
			
	if ( is_front_page() && is_home() ) { ?>
	<h1 itemscope itemtype="http://schema.org/Organization" class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
	<?php } else { ?>
	<div itemscope itemtype="http://schema.org/Organization" class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
	<?php }
				
} 

/**
 * Output page titles, subtitles and archive descriptions
 */
function meteor_page_titles() { ?>
	<h1>
		<?php
			if ( is_category() ) :
				single_cat_title();

			elseif ( is_tag() ) :
				single_tag_title();

			elseif ( is_author() ) :
				the_post();
				printf( __( 'Author: %s', 'haajee' ), '' . get_the_author() . '' );
				rewind_posts();

			elseif ( is_day() ) :
				printf( __( 'Day: %s', 'haajee' ), '<span>' . get_the_date() . '</span>' );

			elseif ( is_month() ) :
				printf( __( 'Month: %s', 'haajee' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

			elseif ( is_year() ) :
				printf( __( 'Year: %s', 'haajee' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

			elseif ( is_404() ) :
				_e( 'Page Not Found', 'haajee' );

			elseif ( is_search() ) :
				printf( __( 'Search Results for: %s', 'haajee' ), '<span>' . get_search_query() . '</span>' );

			// Title for portfolio archive
			elseif ( is_post_type_archive( 'jetpack-portfolio' ) ) :
				 post_type_archive_title();

			// Title for portfolio categories and tags
			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-type' ) ) ) :
					single_term_title();

			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-tag' ) ) ) :
					single_term_title();

									
			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-location' ) ) ) :
					single_term_title();

			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-status' ) ) ) :
					single_term_title();

			elseif ( is_home() || is_page_template( 'templates/template-portfolio-split.php' ) ) :

			elseif ( is_single() ) :
				the_title();
			else :
				single_post_title();

			endif;
		?>
	</h1>

	<?php
	// Get the page excerpt or archive description for a subtitle
	$archive_description = get_the_archive_description();
	$page_excerpt        = get_the_excerpt();

	if ( is_archive() && $archive_description ) {
		$subtitle = get_the_archive_description();
	}

	if ( is_page() && $page_excerpt || is_singular( 'jetpack-portfolio' ) && $page_excerpt ) {
		$subtitle = '<p>' . get_the_excerpt() . '</p>';
	}

	// Get the byline on single post pages
	if ( is_singular( 'post' ) ) {
		meteor_post_byline();
	}
}

/**
 * Output page titles, subtitles and archive descriptions
 */
function haajee_page_titles() { ?>
		<?php
			if ( is_category() ) :
				single_cat_title();

			elseif ( is_tag() ) :
				single_tag_title();

			elseif ( is_author() ) :
				the_post();
				printf( __( 'Author: %s', 'haajee' ), '' . get_the_author() . '' );
				rewind_posts();

			elseif ( is_day() ) :
				printf( __( 'Day: %s', 'haajee' ), '<span>' . get_the_date() . '</span>' );

			elseif ( is_month() ) :
				printf( __( 'Month: %s', 'haajee' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

			elseif ( is_year() ) :
				printf( __( 'Year: %s', 'haajee' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

			elseif ( is_404() ) :
				_e( 'Page Not Found', 'haajee' );

			elseif ( is_search() ) :
				printf( __( 'Search Results for: %s', 'haajee' ), '<span>' . get_search_query() . '</span>' );

			// Title for portfolio archive
			elseif ( is_post_type_archive( 'jetpack-portfolio' ) ) :
				 post_type_archive_title();

			// Title for portfolio categories and tags
			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-type' ) ) ) :
					single_term_title();

			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-tag' ) ) ) :
					single_term_title();
			
			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-location' ) ) ) :
					single_term_title();

			elseif ( is_tax( array(
				'jetpack-portfolio', 'jetpack-portfolio-status' ) ) ) :
					single_term_title();
		
			elseif ( is_home() || is_page_template( 'templates/template-portfolio-split.php' ) ) :

			elseif ( is_single() ) :
				the_title();
			else :
				single_post_title();

			endif;
}


/**
 * Displays post pagination links
 *
 * @since meteor 1.0
 */
function meteor_page_navs( $query = false ) {

	global $wp_query;
	if( $query ) {
		$temp_query = $wp_query;
		$wp_query = $query;
	}

	// Return early if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	} ?>
	<div class="page-navigation">
		<div class="haajee_alignleft"><?php previous_posts_link( '<i class="fa fa-angle-left"></i> <span class="haajee_post-nav-helper">' . __('Vorige', 'haajee' ) . '</span>' ); ?></div>
		<div class="haajee_alignright"><?php next_posts_link( '<span class="haajee_post-nav-helper">' . __('Volgende', 'haajee' ) . '</span> <i class="fa fa-angle-right"></i>' ); ?></div>
	</div>
	<?php
	if( isset( $temp_query ) ) {
		$wp_query = $temp_query;
	}
}

function meteor_post_navs( $query = false ) {
	// Previous/next post navigation.
	$next_post = get_next_post();
	$previous_post = get_previous_post();

	the_post_navigation( array(
		'next_text' => '<div class="haajee_post-nav-text-wrapper"><span class="meta-nav-text meta-title">' . esc_html__( 'Volgend project:', 'haajee' ) . '</span> ' .
		'<span class="screen-reader-text">' . esc_html__( 'Volgend project:', 'haajee' ) . '</span> ' .
		'<span class="post-title">%title</span><i class="fa fa-angle-right"></i></div>',
		'prev_text' => '<div class="haajee_post-nav-text-wrapper"><i class="fa fa-angle-left"></i><span class="meta-nav-text meta-title">' . esc_html__( 'Vorig project:', 'haajee' ) . '</span> ' .
		'<span class="screen-reader-text">' . esc_html__( 'Vorig project:', 'haajee' ) . '</span> ' .
		'<span class="post-title">%title</span></div>',
	) );
}

function meteor_grid_cats() {
	global $post;

	$categories = get_the_category( $post->ID );

	if ( $categories ) {
		// Limit the number of categories output to 2 to keep things tidy on the grid
		$i = 0;

		echo '<div class="grid-cats">';
			foreach( ( get_the_category( $post->ID ) ) as $cat ) {
				echo esc_html( $cat->cat_name );
				if ( ++$i == 2 ) {
					break;
				}
			}
		echo '</div>';
	}
}

function haajee_portfolio_cats() {
	global $post;
	
	echo get_the_term_list(
		$post->ID,
		'jetpack-portfolio-type',
		'<div class="grid-cats">',
		'',
		'</div>'
	);
}

