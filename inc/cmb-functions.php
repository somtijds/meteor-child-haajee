<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'haajee_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/CMB2/CMB2
 */

/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 $cmb CMB2 object.
 *
 * @return bool      True if metabox should show
 */
// function haajee_show_if_front_page( $cmb ) {
// 	// Don't show this metabox if it's not the front page template.
// 	if ( get_option( 'page_on_front' ) !== $cmb->object_id ) {
// 		return false;
// 	}
// 	return true;
// }

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field $field Field object.
 *
 * @return bool              True if metabox should show
 */
// function haajee_hide_if_no_cats( $field ) {
// 	// Don't show this field if not in the cats category.
// 	if ( ! has_tag( 'cats', $field->object_id ) ) {
// 		return false;
// 	}
// 	return true;
// }

add_action( 'cmb2_admin_init', 'haajee_register_project_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function haajee_register_project_metabox() {
	$prefix = 'haajee_demo_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$haajee_cmb_project = new_cmb2_box( array(
		'id'            => $prefix . 'project_',
		'title'         => esc_html__( 'Ouderwetse Metabox', 'haajee' ),
		'object_types'  => array( 'jetpack-portfolio' ), // Post type
		// 'show_on_cb' => 'haajee_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'haajee_add_some_classes', // Add classes through a callback.
	) );

	$haajee_cmb_project->add_field( array(
		'name'       => esc_html__( 'Projectteam', 'haajee' ),
		'desc'       => esc_html__( 'Wie zat er in het projectteam?', 'haajee' ),
		'id'         => $prefix . 'team',
		'type'       => 'text',
	) );

	$haajee_cmb_project->add_field( array(
		'name'       => esc_html__( 'Opdrachtgever', 'haajee' ),
		'desc'       => esc_html__( 'Wie was de opdrachtgever?', 'haajee' ),
		'id'         => $prefix . 'client',
		'type'       => 'text',
	) );

	$haajee_cmb_project->add_field( array(
		'name'       => esc_html__( 'Jaar', 'haajee' ),
		'desc'       => esc_html__( 'Wanneer is dit project gepland of uitgevoerd?', 'haajee' ),
		'id'         => $prefix . 'year',
		'type'       => 'text',
	) );
}