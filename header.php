<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Meteor
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php if ( is_page( 154 ) ) : ?>

		<link rel="preload" href="https://use.typekit.net/wqj7hpu.js" as="script" crossorigin>
		<link rel="preload" href="<?php echo esc_url( get_template_directory_uri() . '/style.css'); ?>" as="style">
		<link rel="preload" href="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/dist/css/meteor-child-haajee.css'); ?>" as="style">

	<?php endif; ?>

	<?php if ( is_page( 90 ) ) : ?>
		
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
		integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
		crossorigin=""/>
		
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
		integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
		crossorigin=""></script>

	<?php endif; ?>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class('tk-eurostile'); ?>>

<?php if ( is_page( 154 ) ) : ?>

	<?php get_template_part('template-parts/haajee-overlay'); ?>

<?php endif; ?>

<header id="masthead" class="site-header">
	<div class="top-navigation">

		<div class="site-identity clear">

			<div class="haajee_stripes">
				<span class="meter"></span>
				<div class="haajee_stripes_inner container">
					<?php if ( ! is_front_page() && ! is_home() ) : ?>

						<div class="haajee_stripes_part haajee_stripes_part--left">
							<h1 class="haajee_page-title"><?php haajee_page_titles(); ?></h1>
						</div>

					<?php endif; ?>
					<!-- Site title and logo -->
					<div class="haajee_stripes_part haajee_stripes_part--right">
						<?php meteor_title_logo(); ?>
					</div>		
				</div>
			</div>

			<?php
				// Forget the mobile menu
				// get_template_part( 'template-parts/content-menu-drawer' );
			?>
			<div class="top-navigation-right container">
				<!-- Main navigation -->
				<nav id="site-navigation" class="main-navigation">
					<?php wp_nav_menu( array(
						'theme_location' => 'primary'
					) );?>
				</nav><!-- .main-navigation -->
			</div><!-- .top-navigation-right -->

		</div><!-- .site-identity-->
	
	</div><!-- .top-navigation -->
	
	<?php if ( ! is_singular('jetpack-portfolio' ) 
		&& ! is_post_type_archive('jetpack-portfolio') 
		&& ! is_front_page()
		) : ?>
		<div class="container text-container haajee_mobile_page-title">
			<div class="header-text">
				<?php meteor_page_titles(); ?>
			</div><!-- .header-text -->
		</div><!-- .text-container -->
	<?php endif; ?>
	
</header><!-- .site-header -->

<?php
// Sticky bar for single pages
if( is_single() ) { ?>
	<nav class="home-nav single-nav">
		<h2 class="sticky-title"><?php the_title(); ?></h2>

		<?php
			// Sharing Buttons
			if ( function_exists( 'sharing_display' ) ) {
				echo sharing_display();
			}
		?>
	</nav>
<?php } ?>
<?php if ( is_page_template('templates/template-portfolio-center-carousel.php') ) : ?>
	<div class="haajee_carousel-wrapper">
		<div class="carousel-navs">
			<div class="haajee_carousel-navs-inner container">
				<button class="carousel-prev haajee_alignleft"><i class="fa fa-angle-left"></i></button>
				<button class="carousel-next haajee_alignright"><i class="fa fa-angle-right"></i></button>
			</div>
		</div>
	</div>
<?php endif; ?>
<div id="page" class="hfeed site container">
	<div id="content" class="site-content">
