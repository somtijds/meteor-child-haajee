<?php 
/**
 * The Sidebar containing the contact page widget
 *
 * @package Meteor Child Haajee
 */

// Get the sidebar widgets
if ( is_active_sidebar( 'haajee_contact_page_widgets' ) ) { ?>
	<aside id="secondary" class="widget-area haajee_widget-area--contact">

		<?php dynamic_sidebar( 'haajee_contact_page_widgets' ); ?>

	</aside><!-- #secondary .widget-area -->
<?php }
