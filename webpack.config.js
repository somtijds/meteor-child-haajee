// webpack.config.js
var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('./assets/dist')
    // public path used by the web server to access the output path
    .setPublicPath('./')
    .setManifestKeyPrefix('./')
    .addStyleEntry('css/meteor-child-haajee', './assets/src/scss/meteor-child-haajee.scss')
    .copyFiles({
        from: './assets/src/js',
        to: 'js/[name].[ext]'
    })
    .copyFiles({
        from: './assets/src/images',
        to: 'images/[name].[ext]'
    })   
    .enableSassLoader()
    .enablePostCssLoader()
    .disableSingleRuntimeChunk()
;

module.exports = Encore.getWebpackConfig();