<?php
/**
 * The template for displaying default pages.
 *
 * @package Meteor
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post();

				// Page content template
				get_template_part( 'template-parts/content-page' );

			endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php if ( is_page('contact') && is_registered_sidebar( 'haajee_contact_page_widgets' ) ) : ?>
		<?php get_sidebar( 'contact-page-widgets' ); ?>
	<?php endif; ?>

	<?php get_footer(); ?>
