//var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();

gulp.task('serve', ['css'], function() {
    browserSync.init({
        // Using localhost sub directories
        proxy: "localhost/haajee"
    });
    gulp.watch('./assets/src/scss/**/*.scss', ['css']);
    gulp.watch('./assets/dist/css/*.css').on('change', browserSync.reload);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
});

gulp.task('sass', function () {
    return gulp.src('./assets/src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/src/css'));
  });

gulp.task('css', ['sass'], function () {
    var plugins = [
        autoprefixer({browsers: ['last 1 version']}),
        cssnano()
    ];
    return gulp.src('./assets/src/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./assets/dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function () {
});

gulp.task('default', ['serve']);