<?php
/**
 * The template used for displaying portfolio items in a grid.
 *
 * @package Meteor
 */
$has_image = has_post_thumbnail() ? 'haajee_has-image' : 'haajee_no-image'; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $has_image ); ?>>

    <div class="haajee_portfolio_item">
        <?php
            $has_image = '';
            if ( has_post_thumbnail() ) :
            $has_image = ' has-image'; ?>
            <div class="featured-image">
                <a href="<?php the_permalink(); ?>" rel="bookmark">
                    <?php the_post_thumbnail( 'meteor-portfolio' ); ?>
                </a>
            </div>
            <?php endif; ?>

        <a href="<?php the_permalink(); ?>" rel="bookmark" class="portfolio-text">
            <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
        </a>
        <?php haajee_portfolio_cats(); ?>
    </div>
    
</article><!-- .post -->
