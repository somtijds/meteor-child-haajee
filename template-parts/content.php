<?php
/**
 * The template used for displaying standard post content
 *
 * @package Meteor
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php meteor_post_media(); ?>

	<?php if ( is_singular('jetpack-portfolio')  ) : ?>

		<label class="haajee_meta-label"><?php echo esc_html__('Project','haajee'); ?></label>
		<?php meteor_page_titles(); ?>
		<?php get_template_part( 'template-parts/content-portfolio-meta' ); ?>
				
	<?php endif; ?>

	<div class="post-content">

		<?php if( ! is_single() ) { ?>
			<header class="entry-header">
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<?php meteor_post_byline(); ?>
			</header>
		<?php } ?>

		<div class="entry-content">

			<?php
			// Remove Jetpack Sharing output
			if( ! is_single() ) {
				meteor_remove_sharing();
			}

			// If it's a video format, filter out the first embed and return the rest of the content
			if ( has_post_format( 'video' ) || has_post_format( 'gallery' ) ) {
				meteor_filtered_content();
			} else {
				the_content( esc_html__( 'Read More', 'meteor' ) );
			}

			if ( is_single()  ) {
	        	// Post meta sidebar
	        	// get_template_part( 'template-parts/content-meta' );

				// Author profile box
				// meteor_author_box();

				// Post navigations
				if( get_next_post() || get_previous_post() ) {
					meteor_post_navs();
				}
				// Comments template
				//comments_template();
			} 
			
			// Post pagination links
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'meteor' ),
				'after'  => '</div>',
			) );
			
			?>
			
		</div><!-- .entry-content -->
	</div><!-- .post-content-->

</article><!-- #post-## -->
