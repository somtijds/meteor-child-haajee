<?php
/**
 * The template used for displaying standard post content
 *
 * @package Meteor
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="post-content">

		<div class="entry-content">

			<?php get_template_part( 'template-parts/content-portfolio-meta' ); ?>

			<?php
			// Remove Jetpack Sharing output
			if( ! is_single() ) {
				meteor_remove_sharing();
			}

			the_content( esc_html__( 'Lees verder...', 'meteor' ) );

			// Post pagination links
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'meteor' ),
				'after'  => '</div>',
			) );

			if ( is_single()  ) {		

				if( get_next_post() || get_previous_post() ) {
					meteor_post_navs();
				}

			} ?>
		</div><!-- .entry-content -->
	</div><!-- .post-content-->

</article><!-- #post-## -->
