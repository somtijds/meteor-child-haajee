<?php
/**
 * The template part for displaying the portfolio meta information
 *
 * @package Meteor Child Haajee
 */
 // Get the Jetpack portfolio tags
$portfolio_tags = get_the_term_list( get_the_ID(), 'jetpack-portfolio-tag', '', _x(', ', '', 'meteor' ), '' );

 // Get the Jetpack portfolio categories
$portfolio_cats = get_the_term_list( get_the_ID(), 'jetpack-portfolio-type', '', _x(', ', '', 'meteor' ), '' );

 // Get the Jetpack portfolio locations
$portfolio_locations = get_the_term_list( get_the_id(), 'jetpack-portfolio-location', '', _x(', ', '', 'meteor' ), '' );

// Get the Jetpack portfolio status
$portfolio_statuses = get_the_term_list( get_the_id(), 'jetpack-portfolio-status', '', _x(', ', '', 'meteor' ), '' );

$portfolio_haajee_meta = get_post_meta( get_the_ID() );
?>

	<ul class="haajee_meta-list">

		    <?php // Portfolio locatie(s)
			 if ( $portfolio_locations ) { ?>
				<label class="haajee_meta-label"><?php echo esc_html_e( 'Locatie', 'haajee' ); ?></label>
				<li>
					<?php echo $portfolio_locations; ?>
				</li>
			<?php } ?>

			<?php
			// Portfolio categories
			if ( $portfolio_cats ) { ?>
				<label class="haajee_meta-label"><?php echo esc_html_e( 'Categorie', 'haajee' ); ?></label>
				<li>
					<?php echo $portfolio_cats; ?>
				</li>
			<?php } ?>

			<?php
			// Portfolio tags
			if ( $portfolio_tags ) { ?>
				<label class="haajee_meta-label"><?php echo esc_html_e( 'Trefwoorden', 'haajee' ); ?></label>
				<li>
					<?php echo $portfolio_tags;	?>
				</li>

			<?php } ?>

			<?php 
			// Portfolio client 
			if ( ! empty( $portfolio_haajee_meta['haajee_demo_client'][0] ) ) : ?>
				<label class="haajee_meta-label"><?php echo esc_html_e( 'Opdrachtgever', 'haajee' ); ?></label>
				<li>
					<?php echo esc_html( $portfolio_haajee_meta['haajee_demo_client'][0] ); ?>
				</li>
			<?php endif; ?>
			
			<?php 
			// Portfolio project team
			if ( ! empty( $portfolio_haajee_meta['haajee_demo_team'][0] ) ) : ?>
				<label class="haajee_meta-label"><?php echo esc_html_e( 'Projectteam', 'haajee' ); ?></label>
				<li>
					<?php echo esc_html( $portfolio_haajee_meta['haajee_demo_team'][0] ); ?>
				</li>
			<?php endif; ?>
			
			<?php 
			// Portfolio year
			if ( ! empty( $portfolio_haajee_meta['haajee_demo_year'][0] ) ) : ?>
				<label class="haajee_meta-label"><?php echo esc_html_e( 'Jaar', 'haajee' ); ?></label>
				<li>
					<?php echo esc_html( $portfolio_haajee_meta['haajee_demo_year'][0] ); ?>
				</li>
			<?php endif; ?>

			<?php
			// Portfolio status
			if ( $portfolio_statuses ) { ?>
				<label class="haajee_meta-label"><?php echo esc_html_e( 'Status', 'haajee' ); ?></label>
				<li>
					<?php echo $portfolio_statuses;	?>
				</li>

			<?php } ?>
	</ul><!-- .meta-list -->
	<label class="haajee_meta-label"><?php echo esc_html_e( 'Info', 'haajee' ); ?></label>
