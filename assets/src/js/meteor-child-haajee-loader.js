(function($) {

	function haajee_loader_animate() {
		$('haajee_overlay').find('.haajee_stripes').addClass('animate');
	}
	
	function haajee_loader_hide() {
		$('.haajee_overlay').hide();
		window.location = document.location.href + '/projecten';
	}
	
	"use strict";

	var $haajeeblocks = $('.section-portfolio-grid');

	$haajeeblocks.imagesLoaded( function() {

		window.setTimeout(haajee_loader_hide, 2000);

	});

})(jQuery);
